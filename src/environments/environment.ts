// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB3pZ0F781dK9V-9w7mY6mkfiBSVII9nO8',
    authDomain: 'ionic-airbnb-dc207.firebaseapp.com',
    databaseURL: 'https://ionic-airbnb-dc207.firebaseio.com',
    projectId: 'ionic-airbnb-dc207',
    storageBucket: 'ionic-airbnb-dc207.appspot.com',
    messagingSenderId: '585418664943',
    appId: '1:585418664943:web:bbb951bf57504d6312241a'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
