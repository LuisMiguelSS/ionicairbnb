import { Component, OnInit } from '@angular/core';
import { Form } from '@angular/forms';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';

@Component({
  selector: 'app-new-place',
  templateUrl: './new-place.page.html',
  styleUrls: ['./new-place.page.scss'],
})
export class NewPlacePage implements OnInit {

  //
  // Attributes
  image: any;
  //
  // Constructor
  constructor() {  }

  ngOnInit() {
  }

  onSubmit(form: Form) {

  }

  async takePicture() {
    const image = Plugins.Camera.getPhoto({
      quality: 100,
      source: CameraSource.Prompt,
      correctOrientation: true,
      width: 300,
      resultType: CameraResultType.Base64
    })
    .then(img => {
      this.image = img.base64String;
    })
    .catch(error => {
      console.log(error);
    });
  }

}
