import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscoverPage } from './discover.page';

const routes: Routes = [
  {
    path: '',
    component: DiscoverPage
  },  {
    path: 'new-place',
    loadChildren: () => import('./new-place/new-place.module').then( m => m.NewPlacePageModule)
  },
  {
    path: 'detail-place',
    loadChildren: () => import('./detail-place/detail-place.module').then( m => m.DetailPlacePageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscoverPageRoutingModule {}
