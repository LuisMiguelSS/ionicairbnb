import { firestore } from 'firebase';
import { PlacesService } from './../Services/places.service';
import { Component, OnInit } from '@angular/core';
import { Place } from '../models/place.model';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit {

  //
  // Attributes
  places: Place[];

  //
  // Constructor
  constructor(
              protected placesService: PlacesService
  ) { }

  ngOnInit() {
    this.refreshPlaces();
  }

  addPlace() {
    this.placesService.addPlace(new Place(null, 'My new place', 'This is such a cool place', '', 30, new firestore.GeoPoint(90, 90)));
    this.refreshPlaces();
  }

  refreshPlaces() {
    this.placesService.getPlaces().subscribe(data => {
      this.places = data.map(e => {
        return {
          id: e.payload.doc.id,
          title: e.payload.doc.get('title'),
          description: e.payload.doc.get('description')
        } as Place;
      });
    });
  }

}
