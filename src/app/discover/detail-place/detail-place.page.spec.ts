import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPlacePage } from './detail-place.page';

describe('DetailPlacePage', () => {
  let component: DetailPlacePage;
  let fixture: ComponentFixture<DetailPlacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPlacePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPlacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
