import { AuthenticatorGuard } from './auth/authenticator.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    // Home
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthenticatorGuard]
  },

  { // Login
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule),
  },

  { // Register
    path: 'register',
    loadChildren: () => import('./auth/register/register.module').then( m => m.RegisterPageModule),
  },

  { // Discover
    path: 'discover',
    children: [
      {
        path: '',
        loadChildren: './discover/discover.module#DiscoverPageModule',
        canLoad: [AuthenticatorGuard]
      },
      {
        path: 'place',
        children: [
          {
            path: '',
            loadChildren: () => import('./discover/detail-place/detail-place.module').then( m => m.DetailPlacePageModule),
            canLoad: [AuthenticatorGuard]
          },
          {
            path: 'new',
            loadChildren: () => import('./discover/new-place/new-place.module').then( m => m.NewPlacePageModule),
            canLoad: [AuthenticatorGuard]
          }
        ]
      }
      // Parametrized routes should be the last ones
    ]
  },

  // Search
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then( m => m.SearchPageModule),
    canLoad: [AuthenticatorGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
