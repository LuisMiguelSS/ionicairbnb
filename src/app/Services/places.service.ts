import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Place } from '../models/place.model';
@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  //
  // Constructor
  constructor(private angularFirestore: AngularFirestore) { }

  //
  // Functions
  addPlace(place: Place) {
    const data = JSON.parse(JSON.stringify(place));
    return this.angularFirestore.collection('places').add(data);
  }

  getPlaces() {
    return this.angularFirestore.collection('places').snapshotChanges();
  }
}
