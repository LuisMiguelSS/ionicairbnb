import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';

export enum AuthResult {
  Success = 1,
  Failed = 0
}
export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  localId: string;
  expiresIn: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})

//
// Class
export class AuthenticatorService {
  userLogged = false;

  constructor(
    private firebaseAuth: AngularFireAuth,
    private alertControl: AlertController,
    private http: HttpClient
  ) {}

  // Log In
  isUserLoggedIn() {
    return this.userLogged;
  }
  logInUser(email: string, password: string) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(
      email,
      password
    );
  }
  logOutUser() {
    this.firebaseAuth.auth.signOut();
    this.userLogged = false;
  }

  // Register
  isUserAlreadyRegistered(email: string) {
    return false;
  }

  async registerUser(email: string, password: string) {
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(
      email,
      password
    );
  }

  showAlert(text: string) {
    this.alertControl
      .create({
        header: 'Authentication failed',
        message: text,
        buttons: ['Okay']
      })
      .then(alertEl => alertEl.present());
  }
}
