import { LoadingController } from '@ionic/angular';
import { Route, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthenticatorService, AuthResult } from '../authenticator.service';
import { NgForm } from '@angular/forms';
import { format } from 'url';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {

  //
  // Attributes
  isLoading = false;

  //
  // Constructor
  constructor(
    private authService: AuthenticatorService,
    private route: Router,
    private loadingControl: LoadingController
  ) {}

  //
  // Functions
  ngOnInit() {}

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    const email = form.value.email;
    const password = form.value.password;

    // Proccess
    this.loadingControl
      .create({ keyboardClose: true, message: 'Registering...' })
      .then(loadingElement => {
        loadingElement.present();

        // Try to register user
        this.authService.registerUser(email, password).then(() => {
          // Success
          form.reset();
          this.goToLogin();

        }).catch((error) => {
          // Error
          this.authService.showAlert(error.message);

        }).finally(() => {
          // Close loading control
          this.loadingControl.dismiss();
        });
      });

  }

  goToLogin() {
    this.route.navigate(['login']);
  }
}
