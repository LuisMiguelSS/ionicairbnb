import { AuthenticatorService, AuthResult } from '../authenticator.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  //
  // Constructor
  constructor(private route: Router,
              private authService: AuthenticatorService,
              private loadingControl: LoadingController) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    const email = form.value.email;
    const password = form.value.password;

    // Proccess
    this.loadingControl
      .create({ keyboardClose: true, message: 'Logging in...' })
      .then(loadingElement => {
        loadingElement.present();

        // Try to login user
        this.authService.logInUser(email, password).then(() => {
          // Success
          form.reset();
          this.authService.userLogged = true;
          this.goToHome();

        }).catch((error) => {
          // Error
          this.authService.showAlert(error.message);

        }).finally(() => {
          // Close loading control
          this.loadingControl.dismiss();
        });
      });

  }

  goToRegister() {
    this.route.navigate(['register']);
  }

  goToHome() {
    this.route.navigate(['home']);
  }

}
