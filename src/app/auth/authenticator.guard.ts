import { AuthenticatorService } from './authenticator.service';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatorGuard implements CanLoad {

  //
  // Constructor
  constructor(private authenticator: AuthenticatorService,
              private router: Router) {
  }

  // CanLoad
  canLoad( route: Route, segments: UrlSegment[])
  : Observable<boolean> | Promise<boolean> | boolean {

    if (this.authenticator.isUserLoggedIn()) {
      return this.authenticator.isUserLoggedIn();

    } else {
      this.router.navigate(['login']);
    }

  }

}
